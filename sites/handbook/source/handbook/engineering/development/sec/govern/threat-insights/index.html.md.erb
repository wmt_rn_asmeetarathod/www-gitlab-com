---
layout: handbook-page-toc
title: Govern, Threat Insights
description: "The Threat Insights group at GitLab is charged with developing solutions to enable customers to manage their security risks effectively and efficiently."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Threat Insights

We are responsible for developing solutions to give customers insights into threats and enable them to manage their security risks effectively and efficiently.

## Common Links

* Slack channel: #g_govern_threat_insights
* Slack alias: @threat_insights_be
* Google groups: eng-dev-secure-threat-insights-members@gitlab.com

## How we work

### Prioritization

We use our [Threat Insights Priorities](https://gitlab.com/gitlab-org/gitlab/-/issues/299275) issue to track what we are doing, and what order to do it in.  Each initiative includes these fields:

1. Name - Description and link to the epic
1. BE DRI / FE DRI - indicates the backend and frontend [DRIs](/handbook/engineering/development/sec/govern/planning/#epic-engineering-dri) who will be actively involved. 
1. Status
   - Not started: No implementation issues should be scheduled. There is likely a Design issue in progress, and there may be issues in planning breakdown. We may have implementation issues if we're planning to start it soon.
   - Started: The scope of the epic should be locked-down, and we have implementation issues scheduled.
   - Complete: Epic is code complete and is in production but may be behind a feature flag. The epic may contain open issues; in this case the issues may be moved to a new epic.
1. Comment - Additional details or capacity needs

Complete items are removed from the table once the code is in production without a feature flag, and a release post, if applicable, has been merged. The epic is closed at this point.

<%= partial "handbook/engineering/metrics/partials/_cross_functional_dashboard.erb", locals: { filter_value: "Threat Insights" } %>

<% if ENV['PERISCOPE_EMBED_API_KEY'] %>
  <div>
    <embed width="100%" height="300" src="<%= signed_periscope_url({ dashboard: 1020520, embed: 'v2', filters: [{name: filter_type_for("quality", "group"), value: filter_value_for('quality', "group", "Threat Insights") }], visible: ["team_group", "stage","development_section"], data_ts: Date.today.to_time.to_i }) %>">
  </div>
  <% else %>
    <p>You must set a <code>PERISCOPE_EMBED_API_KEY</code> environment variable to render this chart.</p>
<% end %>

### Workflow

The Threat Insights group largely follows GitLab's [Product Development Flow](/handbook/product-development-flow/).
Additional information about how we operate can be found on the [Govern Planning page](/handbook/engineering/development/sec/govern/planning/).

#### MR Reviews

We follow these guidelines when submitting MRs for review when the change is within the Threat Insights domain:

1. The initial review should be performed by a member of the team. This helps the team by:
   - Faster reviews, as the reviewer is already familiar with the domain
   - Additional awareness of changes taking place within the domain
   - Identifying changes that don't align with what is happening with the domain 
1. For GraphQL changes, the MR should be reviewed by a frontend engineer as soon as possible. This helps to validate the interface, and allows changes to be made before tests are written.

### Issue Boards

* [Threat Insights Delivery Board](https://gitlab.com/groups/gitlab-org/-/boards/1754666?scope=all&utf8=%E2%9C%93&milestone_title=%23started&label_name[]=group%3A%3Athreat%20insights)
   * Primary board for engineers from which engineers can work. It's stripped down to only include the workflow labels we use when delivering software.

* [Threat Insights Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1420734?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Athreat%20insights)
   * Milestone-centric board primarily used by product management to gauge work in current and upcoming milestones.

* [Threat Insights "Ready to Pull" Board](https://gitlab.com/groups/gitlab-org/-/boards/4643978?label_name[]=group%3A%3Athreat%20insights&label_name[]=ready%20to%20pull)
   * Secondary board for unassigned issues that are separate from a larger effort. Ideal candidates are small features, bugs, and follow-up items. 

## Quality

### How to classify MRs which need to run Package and QA?

It is advisable to manually trigger the `Package and QA` downstream [E2E](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/) job in an MR and review the results when there are changes in:
   - GraphQL (API response, query parameters, schema etc)
   - Gemfile (version changes, adding/removing gems)
   - Vue files which contain "data-qa-selector" attribute because these are used in identifying UI elements in E2E tests
   - Database schema/query changes
   - Any frontend changes which directly impact vulnerability report page, MR security widget, pipeline security tab, security policies, configuration, license compliance page

To manually trigger the QA job:
1. Navigate to the pipelines tab of the MR. 
2. Click the the `>` arrow on the right of the `Stages` and click the `package-and-qa` item.

It's advisable to run the QA job on the latest pipeline at least once during the MR review cycle.

## Monitoring

- [Stage Group dashboad on Grafana](https://dashboards.gitlab.net/d/stage-groups-threat_insights/stage-groups-group-dashboard-secure-threat-insights?orgId=1)

## Largest Contentful Paint (LCP) 

As part of FY22-Q1 OKRs, we've started tracking and monitoring the Largest Contentful Paint for our web pages. The results can be viewed on [this Grafana dashboard](https://dashboards.gitlab.net/d/sftijGFMz/sitespeed-lcp-leaderboard?from=now-90d&orgId=1&to=now&refresh=30s&var-namespace=sitespeed_io&var-path=desktop&var-domains=gitlab_com&var-pages=Secure_Instance_Dashboard_Settings&var-pages=Secure_Instance_Security_Dashboard&var-pages=Secure_Instance_Vulnerability_Report&var-pages=Secure_Group_Security_Dashboard&var-pages=Secure_Group_Vulnerability_Report&var-pages=Secure_Project_Security_Dashboard&var-pages=Secure_Project_Vulnerability_Report&var-pages=Secure_Standalone_Vulnerability&var-browser=chrome&var-connectivity=cable&var-function=median).

## Contributing

### Local testing of licensed features
When a feature needs to check the current license tier, it's important to make sure this also works on GitLab.com.

To emulate this locally, follow these steps:

1. Export an environment variable: `export GITLAB_SIMULATE_SAAS=1`[^1]
1. Within the same shell session run `gdk restart`
1. Admin > Settings > General > "Account and limit", enable "Allow use of licensed EE features"

See the [related handbook entry](https://docs.gitlab.com/ee/development/ee_features.html#act-as-saas) for more details.

### Cross-stack collaboration
We encourage frontend engineers to contribute to the backend and vice versa. In such cases we should work closely with a domain expert from within our group
and also keep the initial review internal. 

This will help ensure that the changes follow best practice, are well tested, have no unintended side effects, and help the team be across any changes that go into the Threat Insights codebase.

### Community Contributions
The Threat Insights grop welcomes community contributions. Any community contribution should get prompt feedback from one of the Threat Insights engineers. All engineers on the team are responsible for working with community contributions. If a team member does not have time to review a community contribution, please tag the Engineering Manager, so that they can assign the community contribution to another team member.

### Office Hours

We hold company-wide office hours alternating Mondays at 3:30pm UTC. Everyone is invited to attend, and it's a great forum to ask questions about Vulnerability Management, customer queries, our road map, and what the Threat Insights team might be thinking about. You can find the [invite](https://calendar.google.com/event?action=TEMPLATE&tmeid=MTF2dGdva3ByNTN2amFzMTltZHZsdXVmaGZfMjAyMjA5MjZUMTUzMDAwWiBnaXRsYWIuY29tX2VkNjIwN3VlbDc4ZGUwajE4NDl2ampuYjNrQGc&tmsrc=gitlab.com_ed6207uel78de0j1849vjjnb3k%40group.calendar.google.com&scp=ALL) on our GitLab Team Meetings shared calendar and take a look at [the agenda](https://docs.google.com/document/d/1CYndmI23HLfrM2zSE8y4V2DtleC6z7bgOZehFxsRnOI/edit#heading=h.1e8qlf7tg4kl). We hope to see you there!

## Footnotes

[^1]: There are many ways to pass an environment variable to your local GitLab instance. For example, you can create a `env.runit` file in the root of your GDK with the above snippet.
